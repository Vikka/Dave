<!--
 * @Author: your name
 * @Date: 2020-06-06 12:46:34
 * @LastEditTime: 2020-06-11 23:10:32
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \nodec:\Users\zhamgzifang\Desktop\code-generation\README.md
--> 
  <div class="homePage" style="line-height: 40px;">
    <h1>代码生成器(框架生成说明介绍)</h1>
    <h3>介绍</h3>
    <p>该套代码生成器仅限于 node框架内的结构生成规则, 所有生成的实现统一在服务端进行处理, 然后返回, 如需要进行修改, 修改后台服务的接口实现即可。</p>
    <h3>内容</h3>
    <p>目前整套的生成内容主要围绕整套框架的设计结构进行, 覆盖后台(WCF接口、数据接口、业务层)、前台的代码生成(View 及ViewModel层), 如下所示。</p>
    <h3>支持</h3>
    <p>
      1.支持生成可预览编辑的代码、直接复制进行移植
      <br />2.直接生成文件, 无论是类库文件、还是前端的UI .xaml文件。
      <br />3.支持代码高亮、方便进行查看编辑
      <br />4.暂只支持mysql但是并不是以后就打算支持mysql 会多种数据库选择
      <br />
    </p>
    <h3>已完成的</h3>
    <p>
      1.数据动态链接，数据结构的管控
      <br />2.表中的数据查看
    </p>
    <h3>未来会完成的</h3>
    <p>
      1.数据可在视图中进行编辑修改。
      <br />2.可通过表/类结构实现api生成
      <br />3.可通过表/类结构实现接口文档生成
      <br />4.可实现操作监控
      <br />6.可管控定时任务
      <br />7.微信小程序/公众号专用区域 - 微信支付快速生成 快速授权 -等jsdk操作
      <br />8.高级用法（内含一键生成代码。）从路由到model dao 一键部署。
      <br />9.文档生成高级用法,数据库联表表查询
      <br />10.多环境切换/多orm供选择
    </p>
  </div>